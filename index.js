/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getUserProfile() {
		let fullName = prompt("Enter your fullname:"); 
		let age = prompt("Enter your age:"); 
		let address = prompt("Enter address:");
	

		console.log(`Hello, ${fullName}`); 
		console.log(`You are , ${age} years old.`); 
		console.log(`You live in ${address}`); 
	}

	getUserProfile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function getFavoriteBands() {
		const favoriteMovies = ['The Beatles', 'Metallica', 'The Eagles', 'Hillsong', 'Planet Shakers']
	

		console.log(`1. ${favoriteMovies[0]}`); 
		console.log(`2. ${favoriteMovies[1]}`); 
		console.log(`3. ${favoriteMovies[2]}`); 
		console.log(`4. ${favoriteMovies[3]}`); 
		console.log(`5. ${favoriteMovies[4]}`); 
	}

	getFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.

*/
	
	//third function here:

	function getFavoriteMoviesAndRating() {
		const FavoriteMoviesAndRating = {
			 firstMovie: "One piece film Red",
			 firstMovieRating: '93%',
			 secondMovie: "Godzilla",
			 secondMovieRating: '92%',
			 thirdMovie: "Avengers Infinity war",
			 thirdMovieRating: '96%',
			 fourthMovie: "JohnWick 3",
			 fourthMovieRating: '95%',
			 fifthMovie: "JohnWick 1",
			 fifthMovieRating: '97%',
		}
	

		console.log(`1. ${FavoriteMoviesAndRating.firstMovie}`); 
		console.log(`Rotten Tomatoes Rating: ${FavoriteMoviesAndRating.firstMovieRating}`); 
		console.log(`2. ${FavoriteMoviesAndRating.secondMovie}`); 
		console.log(`Rotten Tomatoes Rating: ${FavoriteMoviesAndRating.secondMovieRating}`)
		console.log(`3. ${FavoriteMoviesAndRating.thirdMovie}`); 
		console.log(`Rotten Tomatoes Rating: ${FavoriteMoviesAndRating.thirdMovieRating}`)
		console.log(`4. ${FavoriteMoviesAndRating.fourthMovie}`); 
		console.log(`Rotten Tomatoes Rating: ${FavoriteMoviesAndRating.fourthMovieRating}`)
		console.log(`5. ${FavoriteMoviesAndRating.fifthMovie}`); 
		console.log(`Rotten Tomatoes Rating: ${FavoriteMoviesAndRating.fifthMovieRating}`)
	}

	getFavoriteMoviesAndRating();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
// let printFriends = function printUser(){
	let printFriendsFullName = function(){
	alert("Hi! Please add the fullnames of your friends.");
  //let friend1 = alert("Enter your first friend's name:")
	//let friend2 = prom("Enter your second friend's name:")
	//let friend3 = prompt("Enter your third friend's name:")
	let friend1 = prompt("Enter your first friend's fullname:"); 
	let friend2 = prompt("Enter your second friend's fullname:"); 
	let friend3 = prompt("Enter your third friend's fullname:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriendsFullName();
